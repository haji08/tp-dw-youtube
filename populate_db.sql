-- BRAHAM AHMAD - GUEYE EL HADJI AHMADOU - HENRY NATHAN 
insert into advertising (advertising_id, title, format_adv, theme, duration, url_to_display) values (1, 'Boursorama', 'Annonces vidéo désactivables', 'Banque en ligne', 22,'ouvrir-un-compte.boursorama-banque.com/' );
insert into advertising (advertising_id, title, format_adv, theme, duration, url_to_display) values (2, 'Gant Software','Annonces vidéo désactivables', 'Logiciels de gestion d"entreprise', 40, 'clickup.com/' );
insert into advertising (advertising_id, title, format_adv, theme, duration, url_to_display) values (3, 'Ecole de l"IA','Annonces bumper', 'Programmes d"études supèrieurs', 23, 'intelligence-artificielle-school.com/');
insert into advertising (advertising_id, title, format_adv, theme, duration, url_to_display) values (4, 'Unlock Coupons','Annonces vidéo désactivables', 'Navigateurs web', 60, 'joinhoney.com/youtube');
-- advertiser
insert into advertiser (advertiser_id, username, first_name, last_name, gender, age_adv, date_of_birth, country, location_town, adress) values (1, 'Boursorama', 'Benoît', 'Archambault', 'Male', 50, '12/07/1972', 'France', 'Boulogne', 'Boulogne-Billancourt');
insert into advertiser (advertiser_id, username, first_name, last_name, gender, age_adv, date_of_birth, country, location_town, adress) values (2, 'Gant Software', 'Abigale', 'Bigg', 'Agender', 60, '01/07/1962', 'USA', 'New York', 'New York, État de New York, États-Unis');
insert into advertiser (advertiser_id, username, first_name, last_name, gender, age_adv, date_of_birth, country, location_town, adress) values (3, 'Ecole de l"IA', 'Vasily', 'Cluse', 'Bigender', 61, '23/03/1962', 'France', 'Paris', '18 rue du Dôme 92100 Boulogne-Billancourt');
insert into advertiser (advertiser_id, username, first_name, last_name, gender, age_adv, date_of_birth, country, location_town, adress) values (4, 'Unlock Coupon', 'Cece', 'Yearns', 'Male', 34, '10/10/1987', 'USA', 'Californie', 'Los Angeles, CA 90013 ');
-- ad_position
insert into ad_position (position_id, position_label, second_ad, minute_ad, hour_ad) values (1, 'D', 0, 0, 0);
insert into ad_position (position_id, position_label, second_ad, minute_ad, hour_ad) values (2, 'M', 30, 2, 0);
insert into ad_position (position_id, position_label, second_ad, minute_ad, hour_ad) values (3, 'F', 30, 5, 0);
insert into ad_position (position_id, position_label, second_ad, minute_ad, hour_ad) values (4, 'AM', 20, 3, 0);
-- video
insert into video (video_id, title, description,thumbnail, duration, category, playlist, language_vid, recording_date, recording_location, licence) values (1, 'Dinos - Chrome Hearts (Feat Hamza)', 'Clip Vidéo', 'dinos.png', 154, 'music', 'french rap', 'french', '11/05/2022', 'France', 'Universal Music Distribution Deal');
insert into video (video_id, title, description,thumbnail, duration, category, playlist, language_vid, recording_date, recording_location, licence) values (2, '$1 vs $1,000,000 Hotel Room!', 'The hotel at the end is worth the wait!', 'mrbeast.png', 940, 'humor', 'tendance', 'english', '10/02/2022', 'United-states', 'Creative Commons CC BY');
insert into video (video_id, title, description, thumbnail, duration, category, playlist, language_vid, recording_date, recording_location, licence) values (3, 'L"Œuf - Une Nouvelle', 'Une histoire d"Andy Weir', 'Kurzgesagt_In_a_Nutshell.png', 474, 'educative', 'short story', 'english', '09/01/2019', 'Germany', 'Creative Commons CC BY');
-- video_location
insert into video_location (video_location_id, address, full_address, post_box, city, region, country, continent, longitude, latitude, population) values (1, '30, Faculté des Sciences de Montpellier,Place E 34000 Montpellier', 'Bataillon, 34095 Montpellier', 34000, 'Montpellier', 'Hérault', 'France', 'Europe', 3.86852, 43.6134, 295542);
insert into video_location (video_location_id, address, full_address, post_box, city, region, country, continent, longitude, latitude, population) values (2, 'Zweibrückenstraße 12, 80000 Munich', '80331 Munich', 80000, 'Munich', 'Bavière', 'Allemagne', 'Europe', 11.5819806, 1.3443099, 1488202);
insert into video_location (video_location_id, address, full_address, post_box, city, region, country, continent, longitude, latitude, population) values (3, ' C/ de Mallorca, 401 8000 Barcelona', '08013 Barcelona', 8000, 'Barcelona', 'Barcelona', 'Espagne', 'Europe', 11.5819806, 41.403630, 5575000);
-- date_dimension
insert into date_dimension (date_id, date_youtube, full_date_description, day_of_week, calendar_year, calendar_month_name, weekday_indicator, day_number_in_calendar_year, day_number_in_calendar_month, fiscal_year) values (20221106, '06/11/2022', 'Sunday, September 6', 1, 2022, 'Gregorian calendar', 7, 310, 6, 'no');
insert into date_dimension (date_id, date_youtube, full_date_description, day_of_week, calendar_year, calendar_month_name, weekday_indicator, day_number_in_calendar_year, day_number_in_calendar_month, fiscal_year) values (20221107, '07/11/2022', 'Monday, September 7', 2, 2022, 'Gregorian calendar', 2, 311, 7, 'no');
insert into date_dimension (date_id, date_youtube, full_date_description, day_of_week, calendar_year, calendar_month_name, weekday_indicator, day_number_in_calendar_year, day_number_in_calendar_month, fiscal_year) values (20221108, '08/11/2022', 'Tuesday, Septembre 8', 3, 2022, 'Gregorian calendar', 1, 312, 8, 'no');
-- time_dimension
insert into time_dimension (time_id, time_value, am_or_pm, hour_value, hour_number_in_day, minute_value, second_value, hour_12, hour_24) values (1, '06/11/2022 10:33:59', 'pm', 10, 11, 33, 59, 10, 22);
insert into time_dimension (time_id, time_value, am_or_pm, hour_value, hour_number_in_day, minute_value, second_value, hour_12, hour_24) values (2, '07/11/2022 01:10:07', 'am', 1, 2, 33, 59, 1, 1);
insert into time_dimension (time_id, time_value, am_or_pm, hour_value, hour_number_in_day, minute_value, second_value, hour_12, hour_24) values (3, '08/11/2022 23:49:09', 'pm', 11, 24, 49, 9, 11, 23);
-- ad_watching
insert into ad_watching (advertiser_id, date_id, time_id, video_id, ad_position_id, video_location_id, advertising_id, full_view_count, skip_view_count, viewing_time, nb_pub_access) values (1, 20221106, 1, 1, 1, 1, 1, 1876628, 2454657, 10, 162729);          
insert into ad_watching (advertiser_id, date_id, time_id, video_id, ad_position_id, video_location_id, advertising_id, full_view_count, skip_view_count, viewing_time, nb_pub_access) values (2, 20221107, 2, 2, 2, 2, 2, 13268728, 17876203, 25, 262529);
insert into ad_watching (advertiser_id, date_id, time_id, video_id, ad_position_id, video_location_id, advertising_id, full_view_count, skip_view_count, viewing_time, nb_pub_access) values (3, 20221108, 3, 3, 3, 3, 3, 3278828, 2876203, 9, 2214);
insert into ad_watching (advertiser_id, date_id, time_id, video_id, ad_position_id, video_location_id, advertising_id, full_view_count, skip_view_count, viewing_time, nb_pub_access) values (4, 20221107, 2, 2, 1, 3, 2, 54268728, 16878203, 40, 4414);
-- subscriber
insert into subscriber  (subscriber_id,  salutation,  informal_greeting_name, formal_greeting_name, first_name,  middle_name,  surname, birthday, birth_place) values (1, 'Ms', 'Jaden', 'Ms. Anderson', 'Jaden', '', 'Anderson', '06/01/1950', 'Louga');
insert into subscriber  (subscriber_id,  salutation,  informal_greeting_name, formal_greeting_name, first_name,  middle_name,  surname, birthday, birth_place) values (2, 'M', 'John', 'M. Doe', 'John', '', 'Doe', '06/01/1951', 'Washington');
insert into subscriber  (subscriber_id,  salutation,  informal_greeting_name, formal_greeting_name, first_name,  middle_name,  surname, birthday, birth_place) values(3, 'M', 'Nicky', 'Ms. Larson', 'Nicky', '', 'Larson', '05/01/1990', 'Tokyo');
insert into subscriber  (subscriber_id,  salutation,  informal_greeting_name, formal_greeting_name, first_name,  middle_name,  surname, birthday, birth_place) values(4, 'Ms', 'Assita', 'Ms. FROL', 'Assita', '', 'Frol', '06/01/1950', 'Coree du nord');
-- type_subscription
insert into type_subscription(type_subscription_id, name, free_trial, free_trial_duration_min, free_trial_duration_max, price, conditions, description, validity_duration_min, validity_duration_max) values (1, 'Youtube Premium', 'yes', 1, 2, 11.99, 'lorem ipsum dolor sit amet', 'Sunt quia qui quia o.', 1, 3);
insert into type_subscription(type_subscription_id, name, free_trial, free_trial_duration_min, free_trial_duration_max, price, conditions, description, validity_duration_min, validity_duration_max) values (2, 'Youtube Originals', 'yes', 1, 2, 20.99, 'Nulla voluptates con.', 'Quaerat in neque qua.', 1, 2);
-- subscriber_demographic_band
insert into subscriber_demographic_band (subscriber_demographic_band_id,  upper_bound, lower_bound, age_band , gender , classification)
values (1, 14, 0, '0-14ans', 'male', 'Enfant');
insert into subscriber_demographic_band (subscriber_demographic_band_id,  upper_bound, lower_bound, age_band , gender , classification)
values (2, 24, 15, '15-24ans', 'female', 'Enfant');
insert into subscriber_demographic_band (subscriber_demographic_band_id,  upper_bound, lower_bound, age_band, gender, classification)
values (3, 64, 25, '25-64ans', 'male', 'Adultes');
insert into subscriber_demographic_band (subscriber_demographic_band_id,  upper_bound, lower_bound, age_band , gender, classification)
values (4, 64, 25, '25-64ans', 'female', 'Adultes');
-- subscriber_account
insert into subscriber_account (subscriber_account_id, account_status, account_loyalty_level, phone_number) values(1,'ACTIVE', 'MEDIUM', '07 85 24 14 45');
insert into subscriber_account (subscriber_account_id, account_status, account_loyalty_level, phone_number) values(2, 'SUSPENDED', 'NORMAL', '24 574 874');
insert into subscriber_account (subscriber_account_id, account_status, account_loyalty_level, phone_number) values(3, 'ACTIVE', 'HIGH', '07 45 87 98 52');
insert into subscriber_account (subscriber_account_id, account_status, account_loyalty_level, phone_number) values(4, 'ACTIVE', 'MEDIUM', '07 258 474 1');
-- subscriber_location
insert into subscriber_location (subscriber_location_id, street_name, street_number, street_type, street_direction, address, post_box, suite, department, region, city, country_name) values (1,'Main', '494', 'Road', 'South West', '494 Road South west', 2438, '100A', 'Herault', 'Montpellier', 'Montpe', 'France');
insert into subscriber_location (subscriber_location_id, street_name, street_number, street_type, street_direction, address, post_box, suite, department, region, city, country_name) values (2,'6th Street', '900', 'Road', 'North West', '900 6th Street North', 4949, '594ER', 'Dakar', 'Dakar', 'DKFR', 'Senegal');
insert into subscriber_location (subscriber_location_id, street_name, street_number, street_type, street_direction, address, post_box, suite, department, region, city, country_name) values (3,'8th Street', '493', 'Road', 'South West', '493 Road South west', 331, '80A', 'Louga', 'Louga', 'LF', 'Senegal');
insert into subscriber_location (subscriber_location_id, street_name, street_number, street_type, street_direction, address, post_box, suite, department, region, city, country_name) values (4,'10th Street', '478', 'Road', 'South', '478 Road South', 3940, '394A', 'Tunis', 'Tunis', 'TUN', 'Tunisie');
-- subscription
insert into subscription (time_id, date_id, subscriber_id, type_subscription_id, subscriber_demographic_band_id, subscriber_location_id, subscriber_account_id) values (1, 20221106, 1, 1, 1 , 1, 1);
insert into subscription (time_id, date_id, subscriber_id, type_subscription_id, subscriber_demographic_band_id, subscriber_location_id, subscriber_account_id) values (2, 20221106, 2, 2, 3 , 2, 1);
insert into subscription (time_id, date_id, subscriber_id, type_subscription_id, subscriber_demographic_band_id, subscriber_location_id, subscriber_account_id) values (3, 20221107, 2, 2, 1 , 1, 1);
insert into subscription (time_id, date_id, subscriber_id, type_subscription_id, subscriber_demographic_band_id, subscriber_location_id, subscriber_account_id) values (2, 20221108, 1, 1, 1 , 2, 1);

